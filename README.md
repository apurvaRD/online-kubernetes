# Kubernetes

## What is kubernetes?

## Why we use kubernetes?

## Kubernetes Architecture

## Kubernetes Objects:
- Nodes: Worker node
- Pods: Smallest deployable entity of k8s. Wrapper around container. Every pod has its own unique IP address
- Service: Expose application inside / outside of the cluster
    -   ClusterIP: Expose applciation inside the cluster. Used for inside communication only.
    -   NodePort: Expose application outside the cluster using Node Port. Can access application by NODEIP:NODEPORT
    -   LoadBalancer: Expose application outside the cluster on LoadBalancer Service 
- Namespace: Devide the k8s cluster. Segrigate the resources.
- ReplicationController: Responsible for creating replicas of the pods. Used equality based selector. apiVersion: v1
- ReplicaSet: Responsible for creating replicas of the pods. Used set-based selector. Multiple label selector is possible. apiVersion: apps/v1. Set Operators: In, NotIn, Exist
- Deployment: Deployment is responsible for implementing deployment strategies on replicaset.
- StatefulSet: Responsible for managing the replicas of the stateful pod.
- DaemonSet: Responsible for managing the replicas of the pods on every Node
- ConfigMap: To store variables
- Secrets: To store confidentials variables


## Example

Frontend -> Backend -> Database

index.html
Studentapp.war 
mariadb


EKS Cluster
Node group
Docker

### 3-tier modern application deployment
STEP1: Create DATABASE Server and Import Schema

STEP2: Update DATABASE DETAILS in context.xml 
STEP3: Write Dockerfile for backend-app
STEP4: Build Backend-app image and Push to registry

STEP4: Create Deployment.yaml and Service.yaml file for backend
STEP5: Deploy backend-app on cluster

STEP6: Update ClusterIP and Path in proxy.conf
STEP7: Write Dockerfile for frontend-app
STEP8: Build Frontend-app image and Push to Registry

STEP9: Create Deployment.yaml and Service.yaml file for frontend
STEP10: Deploy frontend-app on cluster

-----------
AUTO SCALING IN KUBERNETES

1. Horizontal Pod Autoscaling
2. Vertical Scaling 
3. Node Auto Scaling
-----------------

    
## Create Cluster using KubeAdm

1. Create Control Plane
- Configure Node for K8s

```shell
# dissable swapp memory 
swapoff -a

# Configure Linux Modules 
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system

####### Install Container Runtime ########
### You have to install CRI, RUNC, and CNI
### Installing containerd with package manager will install CRI and RUNC
### You have to install CNI separtely 

# Add docker repo
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# Install and start Containerd
yum install containerd.io -y

# Enable CRI from containerd configuration
# Comment `disabled_plugins = ["cri"]` this configuration 
vi /etc/containerd/config.toml
#sed -i s/'disabled_plugins = ["cri"]'/'# disabled_plugins = ["cri"]'/ /etc/containerd/config.toml

# start and enable containerd
systemctl start containerd
systemctl enable containerd


### Install CNI Plugin ###
yum install wget -y
wget https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-amd64-v1.2.0.tgz
mkdir -p /opt/cni/bin
tar -xzf cni-plugins-linux-amd64-v1.2.0.tgz -C /opt/cni/bin


########### INSTALL KUBEADM, KUBELET, KUBECTL ###########

# Add kuberentes repo
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

yum clean all
yum repolist

# Dissable SELINUX Policy (OR PErmissive)
setenforce 0
vi /etc/selinux/config  
#    selinuxmode=permissive

# Install kubelet, kubeadm, and kubectl
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

# Start kubelet
# Kubelet will restart continuously until kubeadm initialized
systemctl start kubelet
systemctl enable kubelet

# Initialize control-plane
kubeadm init --pod-network-cidr "10.10.0.0/16"


######## Work with kubectl #######
# create kubeconfig file
# admin's kubeconfig file is available in /etc/kubernetes/admin.conf 
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
```

After installing control-plane, you will get the output as:

```shell
# Your Kubernetes control-plane has initialized successfully!

# To start using your cluster, you need to run the following as a regular user:

#   mkdir -p $HOME/.kube
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Alternatively, if you are the root user, you can run:

#   export KUBECONFIG=/etc/kubernetes/admin.conf

# You should now deploy a pod network to the cluster.
# Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
#   https://kubernetes.io/docs/concepts/cluster-administration/addons/

# Then you can join any number of worker nodes by running the following on each as root:

# kubeadm join 172.31.21.252:6443 --token skbiof.7qp4e61uy0gjoift \
#         --discovery-token-ca-cert-hash sha256:70a08e1ce5ba38cac7799a2617139168f61a52fe05b77d13c193164c80d77d6e 
```


Work with cluster from remote location
- Install kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

